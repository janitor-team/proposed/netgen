#!/usr/bin/make -f

export DEB_BUILD_MAINT_OPTIONS = hardening=+all

CPPFLAGS:=$(shell dpkg-buildflags --get CPPFLAGS)
CFLAGS   := $(shell dpkg-buildflags --get CPPFLAGS) $(shell dpkg-buildflags --get CFLAGS)
CXXFLAGS := $(shell dpkg-buildflags --get CPPFLAGS) $(shell dpkg-buildflags --get CXXFLAGS)
LDFLAGS  := $(shell dpkg-buildflags --get LDFLAGS)

DEB_HOST_MULTIARCH ?= $(shell dpkg-architecture -qDEB_HOST_MULTIARCH)

%:
	dh $@ --with=python3 --buildsystem=cmake

export MACHINE=LINUX

extra_flags += \
	-DCMAKE_INSTALL_LIBDIR=usr/lib/${DEB_HOST_MULTIARCH}/netgen \
	-DNG_INSTALL_DIR_LIB:PATH=lib/$(DEB_HOST_MULTIARCH)/netgen \
	-DNG_INSTALL_DIR_INCLUDE:PATH=include/netgen \
	-DPYBIND_INCLUDE_DIR:PATH=/usr/include \
	-DUSE_OCC:BOOL=ON \
	-DUSE_SUPERBUILD:BOOL=OFF \
        -DUSE_NATIVE_ARCH:BOOL=OFF \
	-DENABLE_UNIT_TESTS=ON \
	-DCATCH_INCLUDE_DIR=/usr/include/catch2 \
	-DCMAKE_BUILD_TYPE=RelWithDebInfo

disable_mpi_archs = mips mipsel ppc64el

ifeq (,$(filter $(DEB_HOST_ARCH),$(disable_mpi_archs)))
       extra_flags += \
       -DCMAKE_CXX_FLAGS="-DMPICH_SKIP_MPICXX -lmpi -fPIC \
                          -Wall $(shell dpkg-buildflags --get CXXFLAGS)" \
       -DUSE_MPI:BOOL=ON
else
       extra_flags += \
       -DCMAKE_CXX_FLAGS="-DMPICH_SKIP_MPICXX -fopenmp -fPIC \
                          -Wall $(shell dpkg-buildflags --get CXXFLAGS)" \
       -DUSE_MPI:BOOL=OFF
endif

override_dh_auto_configure:
	dh_auto_configure -- $(extra_flags)
	sed -i s/DEB_HOST_MULTIARCH/${DEB_HOST_MULTIARCH}/ debian/netgen.desktop

override_dh_auto_test:
	dh_auto_install
	cd tests/pytest && \
          PYTHONPATH=$(CURDIR)/debian/tmp/usr/lib/python3/dist-packages \
          LD_LIBRARY_PATH=$(CURDIR)/debian/tmp/usr/lib/$(DEB_HOST_MULTIARCH) \
          python3 -m pytest

override_dh_shlibdeps:
	dh_shlibdeps -l/usr/lib/${DEB_HOST_MULTIARCH}/netgen
